<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use postexus\core\Postexus;

$UserApi = \postexus\modules\user\api\UserApiFactory::getUserApi();
$User = $UserApi->getCurrentUser();

$Request = Postexus::getRequest();

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?php echo 'Postexus'; ?></title>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link rel="stylesheet" href="<?= Postexus::getUri('libraries/bootstrap/css/bootstrap.min.css'); ?>">
		<link rel="stylesheet" href="<?= Postexus::getUri('libraries/bootstrap/css/bootstrap-responsive.min.css'); ?>">
        <link rel="stylesheet" href="<?= Postexus::getUri('templates/default/defaultBackend.css'); ?>">

        <link rel="icon" type="image/png" href="<?= Postexus::getUri('templates/default/images/favicon.png'); ?>">

        <script src="<?= Postexus::getUri('libraries/jquery/jquery-1.7.2.js'); ?>" type="text/javascript"></script>
        <script src="<?= Postexus::getUri('libraries/bootstrap/js/bootstrap.js'); ?>" type="text/javascript"></script>
        <script src="<?= Postexus::getUri('libraries/ckeditor/ckeditor.js'); ?>" type="text/javascript"></script>

        <script type="text/javascript">
            $(function(){
                $('.dropdown-menu form').on('click', function (e) {
                    e.stopPropagation()
                });
            });
        </script>
	</head>

	<body>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?= Postexus::getUri('admin'); ?>">Postexus</a>
                </div>

                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                    <?php
                    // This may be a lame solution, but it's easiest for now
                    /**
                     * @todo Prettify
                     */
                    if ($UserApi->userIsAdmin()) {
                        ?>
                        <li <?php if ($Request->getModule() == 'user') { echo 'class="active"'; } ?>><a href="<?= Postexus::getUri('admin/user/home'); ?>">Users</a></li>
                        <li <?php if ($Request->getModule() == 'page') { echo 'class="active"'; } ?>><a href="<?= Postexus::getUri('admin/page/home'); ?>">Pages</a></li>
                        <li <?php if ($Request->getModule() == 'dns') { echo 'class="active"'; } ?>><a href="<?= Postexus::getUri('admin/dns/home'); ?>">DNS</a></li>
                        <?php
                    }
                    ?>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <?php
                        // This may be a lame solution, but it's easiest for now
                        /**
                         * @todo Prettify
                         */
                        if ($UserApi->userIsGuest()) {
                            ?>
                             <li><a class="btn dropdown-toggle" href="<?= Postexus::getUri('user/login') ?>">Log in</a></li>
                            <?php
                        } else {
                            ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $User->getUsername(); ?> <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Profile</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<?= Postexus::getUri('user/logout') ?>">Sign Out</a></li>
                                </ul>
                            </li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>

            </div>
        </div>

		<div class="container">
            <div class="row">
                <div class="span12">

        <?php /* LOAD AND OUTPUT ALERTS */ ?>