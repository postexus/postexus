<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use postexus\core\Postexus;
use postexus\modules\page\api\PageApiFactory;

$UserApi = \postexus\modules\user\api\UserApiFactory::getUserApi();
$User = $UserApi->getCurrentUser();

$Request = Postexus::getRequest();

$PageApi = PageApiFactory::getPageApi();
$pages = $PageApi->getPages();

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?php echo 'Postexus'; ?></title>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link rel="stylesheet" href="<?= Postexus::getUri('libraries/bootstrap/css/bootstrap.min.css'); ?>">
		<link rel="stylesheet" href="<?= Postexus::getUri('libraries/bootstrap/css/bootstrap-responsive.min.css'); ?>">

        <link rel="icon" type="image/png" href="<?= Postexus::getUri('templates/default/images/favicon.png'); ?>">

        <script src="<?= Postexus::getUri('libraries/jquery/jquery-1.7.2.js'); ?>" type="text/javascript"></script>
        <script src="<?= Postexus::getUri('libraries/bootstrap/js/bootstrap.js'); ?>" type="text/javascript"></script>

        <script type="text/javascript">
            $(function(){
                $('.dropdown-menu form').on('click', function (e) {
                    e.stopPropagation()
                });
            });
        </script>
	</head>

	<body>
        <div class="container">

            <div class="header">
                <ul class="nav nav-pills pull-right">

                <?php

                // TODO Set active page/think of a way to indicate the active page
                foreach ($pages as $Page) {
                ?>
                    <li <?php if($PageApi->getActivePageId() == $Page->getId()) { echo 'class="active"'; } ?> ><a href=" <?= Postexus::getUri('page/view/' . $Page->getId()); ?>"><?= $Page->getTitle(); ?></a></li>
                <?php
                }

                ?>
                </ul>
                <h3 class="text-muted">Postexus</h3>
            </div>

            <hr>

            <div class="row">
                <div class="col-lg-12">

            <?php /* LOAD AND OUTPUT ALERTS */ ?>