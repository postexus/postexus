<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use postexus\core\Postexus;

/**
 * @var \postexus\modules\user\model\User $User;
 */
?>
<p>
    <a class="btn btn-primary" href="<?= Postexus::getUri('admin/user/add'); ?>"><?= _('Add User'); ?></a>
</p>
<table class="table table-striped">
    <thead>
        <tr>
            <th><?= _('User ID'); ?></th>
            <th><?= _('Username'); ?></th>
            <th><?= _('E-mail address'); ?></th>
            <th><?= _('Active?'); ?></th>
            <th><?= _('Admin?'); ?></th>
            <th><?= _('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
    <?php

    if (!empty($users)) {
        foreach ($users as $User) {
            ?>
            <tr <?= ($User->getIsActive() == true) ? '' : 'class="warning"'; ?>'>
                <td><?= $User->getId(); ?></td>
                <td><?= $User->getUsername(); ?></td>
                <td><?= $User->getEmailAddress(); ?></td>
                <td><?= ($User->getIsActive() == true) ? _('Yes') : _('No'); ?></td>
                <td><?= ($User->getIsAdmin() == true) ? _('Yes') : _('No'); ?></td>
                <td><a href="<?= Postexus::getUri('admin/user/view/' . $User->getId()); ?>"><?= _('View'); ?></a> | <a href="<?= Postexus::getUri('admin/user/edit/' . $User->getId()); ?>"><?= _('Edit'); ?></a> | <a href="<?= Postexus::getUri('admin/user/remove/' . $User->getId()); ?>"><?= _('Remove'); ?></a></td>
            </tr>
            <?php
        }
    } else {
        ?>
        <tr>
            <td colspan="4"><?= _('No users found'); ?></td>
        </tr>
        <?php
    }

    ?>
    </tbody>
</table>
