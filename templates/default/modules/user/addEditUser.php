<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use postexus\modules\user\model\User;
/**
 * @var User $User;
 */
?>
<form class="form-horizontal" id="addEditUser" action="" method="post">
	<fieldset id="userData">
		<legend><?= _('Add/edit user'); ?></legend>
		<div class="control-group" id="d_username">
			<label class="control-label" for="username"><?= _('Username'); ?></label>
            <div class="controls">
			    <input class="input-large" id="username" name="username" type="text" value="<?= $User->getUsername(); ?>" />
            </div>
		</div>
		<div class="control-group" id="d_email">
			<label class="control-label" for="email_address"><?= _('E-mail address'); ?></label>
            <div class="controls">
			    <input id="email_address" name="email_address" type="text" value="<?= $User->getEmailAddress(); ?>" />
            </div>
		</div>
		<div class="control-group" id="d_password">
			<label class="control-label" for="password"><?= _('Password'); ?></label>
            <div class="controls">
			    <input id="password" name="password" type="password" value="" />
            </div>
		</div>
		<div class="control-group" id="d_password_confirm">
			<label class="control-label" for="password_confirm"><?= _('Password (confirm)'); ?></label>
            <div class="controls">
			    <input id="password_confirm" name="password_confirm" type="password" value="" />
            </div>
		</div>
        <div class="control-group" id="d_user_is_admin">
            <label class="control-label" for="user_is_admin"><?= _('User is admin'); ?></label>
            <div class="controls">
                <input id="user_is_admin" name="user_is_admin" type="checkbox" <?= $User->getIsAdmin() === true ? 'checked="checked"' : ''; ?> />
            </div>
        </div>
        <div class="control-group" id="d_user_is_active">
            <label class="control-label" for="user_is_active"><?= _('User is active'); ?></label>
            <div class="controls">
                <input id="user_is_active" name="user_is_active" type="checkbox" <?= $User->getIsActive() === true ? 'checked="checked"' : ''; ?> />
            </div>
        </div>
		<div class="form-actions" id="d_actions">
			<input class="btn btn-primary" id="submit" name="submit" type="submit" value="<?= _('Save'); ?>" />
			<a class="btn" href="#"><?= _('Cancel'); ?></a>
		</div>
	</fieldset>
</form>
