<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @var \postexus\modules\user\model\User $User;
 */
?>
<form id="removeUser" action="" method="post">
    <fieldset id="userData">
        <legend><?= _('Remove user'); ?></legend>
        <div class="alert">
            <strong><?= _('Warning!'); ?></strong> <?= _('Are you sure you want to remove the following user'); ?>?
        </div>
        <dl class="dl-horizontal">
            <dt><?= _('ID'); ?></dt>
            <dd><?= $User->getId(); ?></dd>
            <dt><?= _('Username'); ?></dt>
            <dd><?= $User->getUsername(); ?></dd>
            <dt><?= _('E-mail address'); ?></dt>
            <dd><?= $User->getEmailAddress(); ?></dd>
        </dl>
        <div id="d_actions">
            <input class="btn btn-primary" id="submit" name="submit" type="submit" value="<?= _('Remove'); ?>" />
            <a class="btn" href="#"><?= _('Cancel'); ?></a>
        </div>
    </fieldset>
</form>