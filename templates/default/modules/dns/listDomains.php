<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use postexus\core\Postexus;

/**
 * @var \postexus\modules\dns\model\Domain $Domain;
 */
?>
<p>
    <a class="btn btn-primary" href="<?= Postexus::getUri('admin/dns/domain/add'); ?>"><?= _('Add Domain'); ?></a>
</p>
<table class="table table-striped">
    <thead>
        <tr>
            <th><?= _('Domain ID'); ?></th>
            <th><?= _('Domainname'); ?></th>
            <th><?= _('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
    <?php

    if (!empty($domains)) {
        foreach ($domains as $Domain) {
            ?>
            <tr>
                <td><?= $Domain->getId(); ?></td>
                <td><a href="<?= Postexus::getUri('admin/dns/domain/records/' . $Domain->getId()); ?>"><?= $Domain->getDomainName(); ?></a></td>
                <td>
                    <a href="<?= Postexus::getUri('admin/dns/domain/view/' . $Domain->getId()); ?>"><?= _('View'); ?></a> |
                    <a href="<?= Postexus::getUri('admin/dns/domain/records/' . $Domain->getId()); ?>"><?= _('Records'); ?></a> |
                    <a href="<?= Postexus::getUri('admin/dns/domain/edit/' . $Domain->getId()); ?>"><?= _('Edit'); ?></a> |
                    <a href="<?= Postexus::getUri('admin/dns/domain/remove/' . $Domain->getId()); ?>"><?= _('Remove'); ?></a>
                </td>
            </tr>
            <?php
        }
    } else {
        ?>
        <tr>
            <td colspan="4"><?= _('No domains found'); ?></td>
        </tr>
        <?php
    }

    ?>
    </tbody>
</table>
