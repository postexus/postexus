<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use postexus\modules\dns\model\Domain;
use postexus\modules\dns\model\Record;

/**
 * @var Record $Record
 * @var Domain $Domain
 */
?>
<form role="form" id="addEditRecord" action="" method="post">
    <input id="domain_id" name="domain_id" type="hidden" value="<?= $Domain->getId(); ?>" />
    <fieldset id="domainData">
        <legend><?= _('Add/edit record'); ?></legend>
        <div class="form-group" id="d_type">
            <label class="control-label" for="type"><?= _('Type'); ?></label>
            <select id="type" name="type" class="form-control">
                <?php foreach ($Record->getRecordTypes() as $type => $label) { ?>
                    <option value="<?= $type; ?>" <?= $Record->getType() === $type ? 'selected="selected"' : ''; ?>><?= $label; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group" id="d_ttl">
            <label class="control-label" for="ttl"><?= _('TTL'); ?></label>
            <input class="form-control" id="ttl" name="ttl" type="text" value="<?= $Record->getTtl() == null ? $Domain->getMinTtl() : $Record->getTtl(); ?>" />
        </div>
        <div class="form-group" id="d_mx_priority">
            <label class="control-label" for="mx_priority"><?= _('MX Priority'); ?></label>
            <input class="form-control" id="mx_priority" name="mx_priority" type="text" value="<?= $Record->getMxPriority(); ?>" />
        </div>
        <div class="form-group" id="d_host">
            <label class="control-label" for="host"><?= _('Host'); ?></label>
            <input class="form-control" id="host" name="host" type="text" value="<?= $Record->getHost(); ?>" />
        </div>
        <div class="form-group" id="d_content">
            <label class="control-label" for="content"><?= _('Content'); ?></label>
            <input class="form-control" id="content" name="content" type="text" value="<?= $Record->getContent(); ?>" />
        </div>
        <input class="btn btn-primary" id="submit" name="submit" type="submit" value="<?= _('Save'); ?>" />
        <input class="btn btn-info" id="submit-and-add" name="submit-and-add" type="submit" value="<?= _('Save and add another'); ?>" />
        <a class="btn" href="#"><?= _('Cancel'); ?></a>
    </fieldset>
</form>
