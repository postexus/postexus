<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use postexus\modules\dns\model\Domain;
use postexus\modules\user\model\User;

/**
 * @var Domain $Domain
 * @var User[] $users
 * @var User $User
 * @var bool $isAdmin
 */
?>
<form class="form-horizontal" id="addEditDomain" action="" method="post">
	<fieldset id="domainData">
		<legend><?= _('Add/edit domain'); ?></legend>
		<div class="control-group" id="d_domain_name">
			<label class="control-label" for="domain_name"><?= _('Domain name'); ?></label>
            <div class="controls">
			    <input class="input-large" id="domain_name" name="domain_name" type="text" value="<?= $Domain->getDomainName(); ?>" />
            </div>
		</div>
        <div class="control-group" id="d_owner">
            <label class="control-label" for="owner"><?= _('Owner'); ?></label>
            <div class="controls">
                <select id="owner" name="owner">
                    <?php foreach ($users as $User) { ?>
                        <option value="<?= $User->getId(); ?>" <?= $Domain->getOwnerId() === $User->getId() ? 'selected="selected"' : ''; ?>><?= $User->getUsername(); ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="control-group" id="d_soa_email">
            <label class="control-label" for="soa_email"><?= _('SOA e-mail'); ?></label>
            <div class="controls">
                <input class="input-large" id="soa_email" name="soa_email" type="text" value="<?= $Domain->getSoaEmail(); ?>" />
            </div>
        </div>
        <div class="control-group" id="d_refresh">
            <label class="control-label" for="refresh"><?= _('Refresh'); ?></label>
            <div class="controls">
                <input class="input-large" id="refresh" name="refresh" type="text" value="<?= $Domain->getRefresh(); ?>" />
            </div>
        </div>
        <div class="control-group" id="d_retry">
            <label class="control-label" for="retry"><?= _('Retry'); ?></label>
            <div class="controls">
                <input class="input-large" id="retry" name="retry" type="text" value="<?= $Domain->getRetry(); ?>" />
            </div>
        </div>
        <div class="control-group" id="d_expiry">
            <label class="control-label" for="expiry"><?= _('Expiry'); ?></label>
            <div class="controls">
                <input class="input-large" id="expiry" name="expiry" type="text" value="<?= $Domain->getExpiry(); ?>" />
            </div>
        </div>
        <div class="control-group" id="d_min_ttl">
            <label class="control-label" for="min_ttl"><?= _('Minimum TTL'); ?></label>
            <div class="controls">
                <input class="input-large" id="min_ttl" name="min_ttl" type="text" value="<?= $Domain->getMinTtl(); ?>" />
            </div>
        </div>
        <?php if ($isAdmin === true) { ?>
            <div class="control-group" id="d_last_change">
                <label class="control-label" for="last_change"><?= _('Last change'); ?></label>
                <div class="controls">
                    <input class="input-large" id="last_change" name="last_change" type="text" value="<?= $Domain->getLastChange()->format('Y-m-d H:i:s O'); ?>" />
                </div>
            </div>
            <div class="control-group" id="d_serial_count">
                <label class="control-label" for="serial_count"><?= _('Serial count'); ?></label>
                <div class="controls">
                    <input class="input-large" id="serial_count" name="serial_count" type="text" value="<?= $Domain->getSerialCount(); ?>" />
                </div>
            </div>
            <div class="control-group" id="d_needs_update">
                <label class="control-label" for="needs_update"><?= _('Needs update'); ?></label>
                <div class="controls">
                    <input id="needs_update" name="needs_update" type="checkbox" <?= $Domain->getNeedsUpdate() === true ? 'checked="checked"' : ''; ?> />
                </div>
            </div>
        <?php } ?>
		<div class="form-actions" id="d_actions">
			<input class="btn btn-primary" id="submit" name="submit" type="submit" value="<?= _('Save'); ?>" />
			<a class="btn" href="#"><?= _('Cancel'); ?></a>
		</div>
	</fieldset>
</form>
