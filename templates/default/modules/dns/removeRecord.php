<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @var \postexus\modules\dns\model\Record $Record;
 */
?>
<form id="removeRecord" action="" method="post">
    <fieldset id="recordData">
        <legend><?= _('Remove record'); ?></legend>
        <div class="alert">
            <strong><?= _('Warning!'); ?></strong> <?= _('Are you sure you want to remove the following record'); ?>?
        </div>
        <dl class="dl-horizontal">
            <dt><?= _('ID'); ?></dt>
            <dd><?= $Record->getId(); ?></dd>
<!--            <dt>--><?//= _('Record name'); ?><!--</dt>-->
<!--            <dd>--><?//= $Record->getDomainId(); ?><!--</dd>-->
        </dl>
        <div id="d_actions">
            <input class="btn btn-primary" id="submit" name="submit" type="submit" value="<?= _('Remove'); ?>" />
            <a class="btn" href="#"><?= _('Cancel'); ?></a>
        </div>
    </fieldset>
</form>