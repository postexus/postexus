<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use postexus\core\Postexus;
use postexus\modules\dns\model\Record;

/**
 * @var int $domainId
 * @var Record[] $records
 */
?>
<p>
    <a class="btn btn-primary" href="<?= Postexus::getUri('admin/dns/record/add/' . $domainId); ?>"><?= _('Add record'); ?></a>
</p>
<table class="table table-striped">
    <thead>
    <tr>
        <th><?= _('Host'); ?></th>
        <th><?= _('Type'); ?></th>
        <th><?= _('Content'); ?></th>
        <th><?= _('Actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php

    if (!empty($records)) {
        foreach ($records as $Record) {
            ?>
            <tr>
                <td><?= $Record->getHost() ?></td>
                <td><?= $Record->getType(); ?></td>
                <td><?= $Record->getContent() ?></td>
                <td><a href="<?= Postexus::getUri('admin/dns/record/view/' . $Record->getId()); ?>"><?= _('View'); ?></a> | <a href="<?= Postexus::getUri('admin/dns/record/edit/' . $Record->getId()); ?>"><?= _('Edit'); ?></a> | <a href="<?= Postexus::getUri('admin/dns/record/remove/' . $Record->getId()); ?>"><?= _('Remove'); ?></a></td>
            </tr>
        <?php
        }
    } else {
        ?>
        <tr>
            <td colspan="4"><?= _('No records found'); ?></td>
        </tr>
    <?php
    }

    ?>
    </tbody>
</table>
