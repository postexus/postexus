<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @var \postexus\modules\page\model\Page $Page;
 */
?>
<h2><?= $Page->getTitle(); ?></h2>
<dl class="dl-horizontal">
    <dt><?= _('ID'); ?></dt>
    <dd><?= $Page->getId(); ?></dd>
    <dt><?= _('Title'); ?></dt>
    <dd><?= $Page->getTitle(); ?></dd>
    <dt><?= _('Content'); ?></dt>
    <dd><?= $Page->getContent(); ?></dd>
</dl>