<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use postexus\modules\page\model\Page;
/**
 * @var Page $Page;
 */
?>
<form class="form-horizontal" id="addEditPage" action="" method="post">
	<fieldset id="pageData">
		<legend><?= _('Add/edit page'); ?></legend>
		<div class="control-group" id="d_title">
			<label class="control-label" for="title"><?= _('Title'); ?></label>
            <div class="controls">
			    <input class="input-block-level" id="title" name="title" type="text" value="<?= $Page->getTitle(); ?>" />
            </div>
		</div>
		<div class="control-group" id="d_email">
			<label class="control-label" for="content"><?= _('E-mail address'); ?></label>
            <div class="controls">
			    <textarea class="ckeditor" id="content" name="content" rows="10"><?= $Page->getContent(); ?></textarea>
            </div>
		</div>
        <div class="control-group" id="d_page_is_published">
            <label class="control-label" for="page_is_published"><?= _('Page is active'); ?></label>
            <div class="controls">
                <input id="page_is_published" name="page_is_published" type="checkbox" <?= $Page->getIsPublished() === true ? 'checked="checked"' : ''; ?> />
            </div>
        </div>
		<div class="form-actions" id="d_actions">
			<input class="btn btn-primary" id="submit" name="submit" type="submit" value="<?= _('Save'); ?>" />
			<a class="btn" href="#"><?= _('Cancel'); ?></a>
		</div>
	</fieldset>
</form>
