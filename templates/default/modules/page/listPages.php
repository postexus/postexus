<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use postexus\core\Postexus;

/**
 * @var \postexus\modules\page\model\Page $Page;
 */
?>
<p>
    <a class="btn btn-primary" href="<?= Postexus::getUri('admin/page/add'); ?>"><?= _('Add Page'); ?></a>
</p>
<table class="table">
    <thead>
        <tr>
            <th><?= _('Page ID'); ?></th>
            <th><?= _('Title'); ?></th>
            <th><?= _('Published?'); ?></th>
            <th><?= _('Last changed'); ?></th>
            <th><?= _('Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
    <?php

    if (!empty($pages)) {
        foreach ($pages as $Page) {
            ?>
            <tr <?= ($Page->getIsPublished() == false) ? '' : 'class="success"'; ?>>
                <td><?= $Page->getId(); ?></td>
                <td><?= $Page->getTitle(); ?></td>
                <td><?= ($Page->getIsPublished() == true) ? _('Yes') : _('No'); ?></td>
                <td><?= $Page->getLastChange()->format('Y-m-d H:i:s'); ?></td>
                <td><a href="<?= Postexus::getUri('admin/page/view/' . $Page->getId()); ?>"><?= _('View'); ?></a> | <a href="<?= Postexus::getUri('admin/page/edit/' . $Page->getId()); ?>"><?= _('Edit'); ?></a> | <a href="<?= Postexus::getUri('admin/page/remove/' . $Page->getId()); ?>"><?= _('Remove'); ?></a></td>
            </tr>
            <?php
        }
    } else {
        ?>
        <tr>
            <td colspan="4"><?= _('No pages found'); ?></td>
        </tr>
        <?php
    }

    ?>
    </tbody>
</table>
