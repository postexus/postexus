<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\helpers\alert;

class Alert
{
    const TYPE_SUCCESS  = 'success';
    const TYPE_INFO     = 'info';
    const TYPE_WARNING  = 'warning';
    const TYPE_ERROR    = 'error';

    /** @var string */
    private $type;
    /** @var string */
    private $text;

    /**
     * @param string $type One of the TYPE_ constants from this class
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}