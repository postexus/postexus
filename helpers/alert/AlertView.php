<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\helpers\alert;

class AlertView
{
    private $Alert;

    public function outputSuccess()
    {
        $Alert = $this->Alert;
        include(BASEDIR . '/templates/default/helpers/alert/success.php');
    }

    public function outputInfo()
    {
        $Alert = $this->Alert;
        include(BASEDIR . '/templates/default/helpers/alert/info.php');
    }

    public function outputWarning()
    {
        $Alert = $this->Alert;
        include(BASEDIR . '/templates/default/helpers/alert/warning.php');
    }

    public function outputError()
    {
        $Alert = $this->Alert;
        include(BASEDIR . '/templates/default/helpers/alert/error.php');
    }

    public function setAlert($Alert)
    {
        $this->Alert = $Alert;
    }
}