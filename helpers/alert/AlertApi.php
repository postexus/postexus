<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\helpers\alert;

/**
 * Class AlertApi
 * @package postexus\helpers\alert
 * @todo Rename into AlertApi
 */
class AlertApi
{
    private $alerts = array();

    public function outputAlerts()
    {
        /**
         * @var Alert $Alert
         */
        foreach ($this->alerts as $Alert) {
            $View = new AlertView();
            $View->setAlert($Alert);

            if ($Alert->getType() == Alert::TYPE_SUCCESS) {
                $View->outputSuccess();
            } elseif ($Alert->getType() == Alert::TYPE_INFO) {
                $View->outputInfo();
            } elseif ($Alert->getType() == Alert::TYPE_WARNING) {
                $View->outputWarning();
            } elseif ($Alert->getType() == Alert::TYPE_ERROR) {
                $View->outputError();
            }
        }
    }

    public function addAlert($type, $text)
    {
        $Alert = new Alert($type);
        $Alert->setText($text);
        $this->alerts[] = $Alert;
    }

    public function postponeAlerts()
    {
        // TODO Save alerts to cookies and load them for display at the start of the next request
    }
}