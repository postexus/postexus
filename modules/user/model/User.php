<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\user\model;

use postexus\core\base\Object;

class User extends Object
{
    /** @var null|int */
    protected $id = null;
    /** @var string */
    protected $username;
    /** @var string */
    protected $email_address;
    /** @var string */
    protected $password;
    /** @var string */
    protected $newPassword;
    /** @var string */
    protected $newPasswordConfirmation;
    /** @var bool */
    protected $is_admin = false;
    /** @var bool */
    protected $is_active = false;

	// TODO Move this to the Repository?
    /** @var array */
    public $fieldMapping = array(
        'username' => 'username',
        'emailAddress' => 'email_address',
        'password' => 'password',
        'isAdmin' => 'is_admin',
        'isActive' => 'is_active'
    );

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->email_address;
    }

    /**
     * @param string $password
     */
    public function setNewPassword($password)
    {
        $this->newPassword = $password;
    }

    /**
     * @param string $password
     */
    public function setNewPasswordConfirmation($password)
    {
        $this->newPasswordConfirmation = $password;
    }

    /**
     * @param string $email_address
     */
    public function setEmailAddress($email_address)
    {
        $this->email_address = $email_address;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param bool $isAdmin
     */
    public function setIsAdmin($isAdmin)
    {
        $this->is_admin = $isAdmin;
    }

    /**
     * @return bool
     */
    public function getIsAdmin()
    {
        return (bool) $this->is_admin;
    }

    /**
     * @param bool $is_active
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return (bool) $this->is_active;
    }

    /**
     * @return bool
     */
    public function validate()
    {
        if (empty($this->username)) {
            // Username is empty
            //_('Username may not be empty');
            return false;
        }

        if (empty($this->email_address)) {
            // E-mail address is empty
            //_('E-mail address may not be empty');
            return false;
        }

        if (!empty($this->newPassword) && !empty($this->newPasswordConfirmation)) {
            if ($this->newPassword === $this->newPasswordConfirmation) {
                $this->password = password_hash($this->newPassword, PASSWORD_BCRYPT);
            } else {
                // Passwords do not match
                //_('Your passwords do not match');
                return false;
            }
        } else {
            // Password is empty or no password confirmation was given
            //_('Please provide your password (twice)');
            return false;
        }

        return true;
    }
}
