<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\user\view;

use postexus\helpers\alert\Alert;
use postexus\helpers\alert\AlertApi;
use postexus\modules\user\model\User;

class UserView
{
	/** @var Alert */
	private $Alert;

    /**
     * @param \postexus\helpers\alert\Alert|\postexus\helpers\alert\AlertApi $Alert $Alert
     */
	public function __construct(AlertApi $Alert)
	{
		$this->Alert = $Alert;
	}

    /**
     * @param array $users
     */
    public function home($users)
    {
        require_once(BASEDIR . '/templates/default/modules/user/listUsers.php');
    }

    /**
     * @param User $User
     */
    public function view(User $User)
    {
        require_once(BASEDIR . '/templates/default/modules/user/viewUser.php');
    }

    /**
     * @param User $User
     */
    public function addEdit(User $User)
    {
        require_once(BASEDIR . '/templates/default/modules/user/addEditUser.php');
    }

    /**
     * @param User $User
     */
    public function remove(User $User)
    {
        require_once(BASEDIR . '/templates/default/modules/user/removeUser.php');
    }

    public function login()
    {
        require_once(BASEDIR . '/templates/default/modules/user/login.php');
    }

    public function register()
    {
        require_once(BASEDIR . '/templates/default/modules/user/registerUser.php');
    }

    public function saveSuccess()
    {
		$this->Alert->addAlert(Alert::TYPE_SUCCESS, _('User successfully saved!'));
    }

    public function saveFailure()
    {
        $this->Alert->addAlert(Alert::TYPE_ERROR, _('User could not be saved!'));
    }

    public function removeSuccess()
    {
        $this->Alert->addAlert(Alert::TYPE_SUCCESS, _('User successfully removed!'));
    }

    public function removeFailure()
    {
        $this->Alert->addAlert(Alert::TYPE_ERROR, _('User could not be removed!'));
    }

    public function loginSuccess()
    {
        $this->Alert->addAlert(Alert::TYPE_SUCCESS, _('Login successful!'));
    }

    public function loginFailure()
    {
        $this->Alert->addAlert(Alert::TYPE_ERROR, _('Login failed!'));
    }

    public function logoutSuccess()
    {
        $this->Alert->addAlert(Alert::TYPE_SUCCESS, _('Logout successful!'));
    }

    public function logoutFailure()
    {
        $this->Alert->addAlert(Alert::TYPE_ERROR, _('Logout failed!'));
    }

    public function spamFailure()
    {
        $this->Alert->addAlert(Alert::TYPE_ERROR, _('SPAM registration detected.'));
    }
}

?>
