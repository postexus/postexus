<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\user\controller;

use Exception;
use postexus\core\Postexus;
use postexus\core\Response;
use postexus\modules\user\api\UserApi;
use postexus\modules\user\api\UserApiFactory;
use postexus\modules\user\model\User;
use postexus\modules\user\view\UserView;

/**
 * Class UserController
 * @package postexus\modules\user\controller
 * @todo Permissions
 */
class UserController
{
	/** @var UserApi */
	private $UserApi;
	/** @var UserView */
	private $UserView;
    /** @var Response */
    private $Response;

    const ACTION_ADDEDIT = 'addedit';
    const ACTION_REGISTER = 'register';

    public function __construct()
	{
		$this->UserApi = UserApiFactory::getUserApi();
		$this->UserView = new UserView(Postexus::getAlert());
        $this->Response = Postexus::getPostexus()->getResponse();
	}

	public function home()
	{
        if ($this->UserApi->userIsAdmin()) {
            $users = $this->UserApi->getUsers();
            $this->UserView->home($users);
        }
	}

    /**
     * @param int|null $userId
     */
    public function addEdit($userId = null)
    {
        if ($this->UserApi->userIsAdmin()) {
            if (is_null($userId)) {
                $User = $this->UserApi->createUser();
            } else {
                $User = $this->UserApi->getUserById($userId);
            }

            if (isset($_POST['submit'])) {
                $this->loadFormDataInUser($this->sanitizeFormData($_POST, self::ACTION_ADDEDIT), $User);

                try {
                    $this->UserApi->saveUser($User);
                    $this->UserView->saveSuccess();
                } catch (Exception $E) {
                    $this->UserView->saveFailure();
                }
                return;
            }

            $this->UserView->addEdit($User);
            return;
        }
    }

    /**
     * @param int $userId
     */
    public function remove($userId)
    {
        if ($this->UserApi->userIsAdmin()) {
            $User = $this->UserApi->getUserById($userId);

            if (isset($_POST['submit'])) {
                try {
                    $this->UserApi->removeUser($User);
                    $this->UserView->removeSuccess();
                } catch (Exception $E) {
                    $this->UserView->removeFailure();
                }
                return;
            }

            $this->UserView->remove($User);
            return;
        }
    }

    /**
     * @param int $userId
     */
    public function view($userId)
    {
        if ($this->UserApi->userIsAdmin()) {
            $User = $this->UserApi->getUserById($userId);

            $this->UserView->view($User);
            return;
        }
    }

    public function login()
    {
        if (isset($_POST['submit'])) {
            $User = $this->UserApi->getUserByUsername($_POST['username']);
            if ($this->UserApi->login($User)) {
                $this->UserView->loginSuccess();
                $this->Response->seeOther(Postexus::getUri('admin'));
                return;
            } else {
                $this->UserView->loginFailure();
            }
        }

        $this->UserView->login();
    }

    public function logout()
    {
        if ($this->UserApi->logout()) {
            $this->UserView->logoutSuccess();
            return;
        }

        $this->UserView->logoutFailure();
    }

    public function register()
    {
        if ($this->UserApi->userIsGuest()) {
            $User = $this->UserApi->createUser();

            if (isset($_POST['submit']) && $this->registrationIsSpam($_POST) === false) {
                $this->loadFormDataInUser($this->sanitizeFormData($_POST, self::ACTION_REGISTER), $User);

                $this->UserApi->saveUser($User);
                $this->UserView->saveSuccess();
                return;
            } elseif ($this->registrationIsSpam($_POST) == true) {
                $this->UserView->spamFailure();
            }

            $this->UserView->register();
            return;
        }
    }

    /**
     * @param array $formData
     * @param User $User
     */
    private function loadFormDataInUser(array $formData, User $User)
	{
        if (isset($formData['username'])) {
            $User->setUsername($formData['username']);
        }

        if (isset($formData['email_address'])) {
            $User->setEmailAddress($formData['email_address']);
        }

        if (isset($formData['password'])) {
            $User->setNewPassword($formData['password']);
        }

        if (isset($formData['password_confirm'])) {
            $User->setNewPasswordConfirmation($formData['password_confirm']);
        }

        if (isset($formData['user_is_admin'])) {
            $User->setIsAdmin(true);
        } else {
            $User->setIsAdmin(false);
        }

        if (isset($formData['user_is_active'])) {
            $User->setIsActive(true);
        } else {
            $User->setIsActive(false);
        }
	}

    /**
     * @param $postData
     * @param $action
     * @return array
     */
    private function sanitizeFormData($postData, $action)
    {
        switch ($action) {
            case self::ACTION_ADDEDIT:
                $allowedKeys = array_flip(array(
                    'username',
                    'email_address',
                    'password',
                    'password_confirm',
                    'user_is_admin',
                    'user_is_active'
                ));
                break;

            case self::ACTION_REGISTER:
                $allowedKeys = array_flip(array(
                    'username',
                    'email_address',
                    'password',
                    'password_confirm'
                ));
                break;

            default:
                $allowedKeys = array();
                break;
        }

        return array_intersect_key($postData, $allowedKeys);
    }

    /**
     * @param array $postData
     * @return bool
     */
    private function registrationIsSpam(array $postData)
    {
        if (isset($postData['email_address_confirm']) && !empty($postData['email_address_confirm'])) {
            return true;
        }

        return false;
    }
}