<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\user\api;

use postexus\core\Config;
use postexus\modules\user\model\User;
use postexus\modules\user\repository\UserRepository;

class UserApi
{
	/** @var UserRepository */
	private $UserRepository;
    /** @var Config */
    private $Config;
    /** @var User Current User that is logged in  */
    private static $CurrentUser;

    /**
     * @param UserRepository $UserRepository
     * @param Config $Config
     */
    public function __construct(UserRepository $UserRepository, Config $Config)
	{
		$this->UserRepository = $UserRepository;
        $this->Config = $Config;
	}

    /**
     * @return User
     */
    public function createUser()
	{
		return $this->UserRepository->createUser();
	}

	/**
	 * @param User $User
     * @return bool
     */
	public function saveUser(User $User)
	{
		if (!$User->validate()) {
            // TODO Display errors
        }

		return $this->UserRepository->saveUser($User);
	}

    /**
     * @param User $User
     * @return bool
     */
    public function removeUser(User $User)
    {
        if ($User->getId() != null) {
            return $this->UserRepository->removeUser($User);
        }

        return false;
    }

    /**
     * Returns an array of x users, ordered by ID
     * @param int $numberOfUsers
     * @return User[]
     */
    public function getUsers($numberOfUsers = 20)
    {
        return $this->UserRepository->getObjects($numberOfUsers);
    }

    /**
     * @param int $userId
     * @return User
     */
    public function getUserById($userId)
    {
        return $this->UserRepository->getObjectById($userId);
    }

    /**
     * @param string $username
     * @return User
     */
    public function getUserByUsername($username)
    {
        return $this->UserRepository->getUserByUsername($username);
    }

    /**
     * @param User $User
     * @return bool
     */
    public function login(User $User)
    {
        if (password_verify($_POST['password'], $User->getPassword()) && $User->getIsActive() === true) {
            setcookie('postexusLogin', $User->getUsername() . ':' . $User->getPassword(), time() + 432000, '/' . $this->Config->getUrlPrefix(), $this->Config->getCookieDomain());
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function logout()
    {
        if (!$this->userIsGuest(self::$CurrentUser)) {
            return setcookie('postexusLogin', '', 1, '/' . $this->Config->getUrlPrefix(), $this->Config->getCookieDomain());
        }

        return true;
    }
    
    public function checkLogin()
    {
        if (isset($_COOKIE['postexusLogin'])) {
            $cookieData = explode(':', $_COOKIE['postexusLogin']);

            if (isset($cookieData[0]) && isset($cookieData[1])) {
                $User = $this->UserRepository->getUserByUsername($cookieData[0]);

                if ($User->getPassword() === $cookieData[1]) {
                    self::$CurrentUser = $User;
                    return;
                }
            }
        }

        self::$CurrentUser = $this->createUser();
        return;
    }

    /**
     * @return User
     */
    public function getCurrentUser()
    {
        if (!isset(self::$CurrentUser)) {
            $this->checkLogin();
        }

        return self::$CurrentUser;
    }

    /**
     * @param User $User
     * @return bool
     */
    public function userIsAdmin(User $User = null)
    {
        if (is_null($User)) {
            $User = $this->getCurrentUser();
        }

        return $User->getIsAdmin() === true;
    }

    /**
     * @param User $User
     * @return bool
     */
    public function userIsGuest(User $User = null)
    {
        if (is_null($User)) {
            $User = $this->getCurrentUser();
        }

        return $User->getId() === null;
    }
}
