<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\user\repository;

use Exception;
use PDO;
use postexus\core\base\Repository;
use postexus\modules\user\model\User;

class UserRepository extends Repository
{
    /**
     * @return User
     */
    public function createUser()
	{
		return new User();
	}

	/**
	 * @param User $User
     * @return bool
     * @throws Exception
	 */
	public function saveUser(User $User)
	{
        // Prepare statement
		if ($User->getId() !== null) {
			$Statement = $this->Database->prepare("UPDATE " . $this->getTableName() . "
                                       SET " . $this->createUpdateClause($User->fieldMapping) . "
                                       WHERE id = :id");
            $Statement->bindValue(':id', $User->getId());
		} else {
			$Statement = $this->Database->prepare("INSERT INTO " . $this->getTableName() . " (" . implode(',', $User->fieldMapping) . ")
                                       VALUES (:" . implode(', :', $User->fieldMapping) . ")");
		}

		// Bind parameters
		foreach ($User->fieldMapping as $objectField => $databaseField) {
            $method = 'get' . ucfirst($objectField);
            $value = $User->$method();

            if (is_bool($value)) {
                $value = (int) $value;
            }

            $Statement->bindValue(':' . $databaseField, $value);
		}

		if (!$Statement->execute()) {
			throw new Exception($Statement->errorCode() . ':' . $Statement->errorInfo());
		}

        return true;
	}

    /**
     * @param User $User
     * @return bool
     * @throws \Exception
     */
    public function removeUser(User $User)
    {
        $Statement = $this->Database->prepare("DELETE
                FROM " . $this->getTableName() . "
                WHERE id = :id");
        $Statement->bindValue(':id', $User->getId());

        if (!$Statement->execute()) {
            throw new Exception($Statement->errorCode() . ':' . $Statement->errorInfo());
        }

        return true;
    }

    /**
     * @param string $username
     * @return User
     */
    public function getUserByUsername($username)
    {
        $Statement = $this->Database->prepare("SELECT *
                FROM " . $this->getTableName() . "
                WHERE username = :username");
        $Statement->bindValue(':username', $username);

        $Statement->execute();

        $Statement->setFetchMode(PDO::FETCH_CLASS, $this->getClassName());

        return $Statement->fetch();
    }

    /**
     * @return string
     */
    protected function getTableName()
    {
        return '"user"';
    }

    /**
     * @return string
     */
    protected function getClassName()
    {
        return "\\postexus\\modules\\user\\model\\User";
    }


}