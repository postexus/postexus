<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\page\repository;

use DateTime;
use Exception;
use postexus\core\base\Repository;
use postexus\modules\page\model\Page;

class PageRepository extends Repository
{
    /**
     * @return Page
     */
    public function createPage()
	{
		return new Page();
	}

	/**
	 * @param Page $Page
     * @return bool
     * @throws Exception
     * @todo Move some of this stuff to a base repository?
	 */
	public function savePage(Page $Page)
	{
        // Prepare statement
		if ($Page->getId() !== null) {
			$Statement = $this->Database->prepare("UPDATE " . $this->getTableName() . "
                                       SET " . $this->createUpdateClause($Page->fieldMapping) . "
                                       WHERE id = :id");
            $Statement->bindValue(':id', $Page->getId());
		} else {
			$Statement = $this->Database->prepare("INSERT INTO " . $this->getTableName() . " (" . implode(',', $Page->fieldMapping) . ")
                                       VALUES (:" . implode(', :', $Page->fieldMapping) . ")");
		}

		// Bind parameters
		foreach ($Page->fieldMapping as $objectField => $databaseField) {
            $method = 'get' . ucfirst($objectField);
            $value = $Page->$method();

            if (is_bool($value)) {
                $value = (int) $value;
            } elseif ($value instanceof DateTime) {
                $value = $value->format('Y-m-d H:i:s O');
            }

            $Statement->bindValue(':' . $databaseField, $value);
		}

		if (!$Statement->execute()) {
            var_dump($Statement->errorInfo());
			throw new Exception($Statement->errorCode() . ':' . $Statement->errorInfo());
		}

        return true;
	}

    /**
     * @param Page $Page
     * @return bool
     */
    public function removePage(Page $Page)
    {
        $Statement = $this->Database->prepare("DELETE
                FROM " . $this->getTableName() . "
                WHERE id = :id");
        $Statement->bindValue(':id', $Page->getId());

        return $Statement->execute();
    }

    /**
     * @return string
     */
    protected function getTableName()
    {
        return 'page';
    }

    /**
     * @return string
     */
    protected function getClassName()
    {
        return "\\postexus\\modules\\page\\model\\Page";
    }
}