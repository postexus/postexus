<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\page\view;

use postexus\helpers\alert\Alert;
use postexus\helpers\alert\AlertApi;
use postexus\modules\page\model\Page;

class PageView
{
	/** @var Alert */
	private $Alert;

    /**
     * @param \postexus\helpers\alert\Alert|\postexus\helpers\alert\AlertApi $Alert $Alert
     * @todo Alert should be the AlertApi
     */
	public function __construct(AlertApi $Alert)
	{
		$this->Alert = $Alert;
	}

    /**
     * @param array $pages
     */
    public function home($pages)
    {
        require_once(BASEDIR . '/templates/default/modules/page/listPages.php');
    }

    /**
     * @param Page $Page
     */
    public function view(Page $Page)
    {
        require_once(BASEDIR . '/templates/default/modules/page/viewPage.php');
    }

    /**
     * @param Page $Page
     */
    public function addEdit(Page $Page)
    {
        require_once(BASEDIR . '/templates/default/modules/page/addEditPage.php');
    }

    /**
     * @param Page $Page
     */
    public function remove(Page $Page)
    {
        require_once(BASEDIR . '/templates/default/modules/page/removePage.php');
    }

    public function saveSuccess()
    {
		$this->Alert->addAlert(Alert::TYPE_SUCCESS, _('Page successfully saved!'));
    }

    public function saveFailure()
    {
		$this->Alert->addAlert(Alert::TYPE_ERROR, _('Page could not be saved!'));
    }

    public function removeSuccess()
    {
        $this->Alert->addAlert(Alert::TYPE_SUCCESS, _('Page successfully removed!'));
    }

    public function removeFailure()
    {
        $this->Alert->addAlert(Alert::TYPE_ERROR, _('Page could not be removed!'));
    }
}

?>
