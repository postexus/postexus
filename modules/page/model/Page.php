<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\page\model;

use DateTime;
use postexus\core\base\Object;

class Page extends Object
{
    /** @var null|int */
    protected $id = null;
    /** @var int */
    protected $author_id;
    /** @var string */
    protected $content;
    /** @var string */
    protected $title;
    /** @var DateTime */
    protected $last_change;
    /** @var bool */
    protected $is_published = false;

	// TODO Move this to the Repository?
    /** @var array */
    public $fieldMapping = array(
        'authorId' => 'author_id',
        'title' => 'title',
        'content' => 'content',
        'lastChange' => 'last_change',
        'isPublished' => 'is_published'
    );

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $author_id
     */
    public function setAuthorId($author_id)
    {
        $this->author_id = (int) $author_id;
    }

    /**
     * @return int
     */
    public function getAuthorId()
    {
        return (int) $this->author_id;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param boolean $is_published
     */
    public function setIsPublished($is_published)
    {
        $this->is_published = (bool) $is_published;
    }

    /**
     * @return boolean
     */
    public function getIsPublished()
    {
        return (bool) $this->is_published;
    }

    /**
     * @param DateTime $last_change
     */
    public function setLastChange(DateTime $last_change)
    {
        $this->last_change = $last_change;
    }

    /**
     * @return DateTime
     */
    public function getLastChange()
    {
        if ($this->last_change instanceof DateTime) {
            return $this->last_change;
        } else {
            if (!empty($this->last_change)) {
                $this->last_change = new DateTime($this->last_change);
            } else {
                $this->last_change = new DateTime();
            }
        }

        return $this->last_change;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function validate()
    {
        if (empty($this->author_id)) {
            // Author is empty
            //_('Author may not be empty');
            return false;
        }

        if (empty($this->title)) {
            // Title is empty
            //_('Title may not be empty');
            return false;
        }

        if (empty($this->content)) {
            // Content is empty
            //_('Content may not be empty');
            return false;
        }

        return true;
    }
}
