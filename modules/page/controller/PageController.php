<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\page\controller;

use Exception;
use postexus\core\Postexus;
use postexus\modules\page\api\PageApi;
use postexus\modules\page\api\PageApiFactory;
use postexus\modules\page\model\Page;
use postexus\modules\page\view\PageView;
use postexus\modules\user\api\UserApi;
use postexus\modules\user\api\UserApiFactory;

/**
 * Class PageController
 * @package postexus\modules\page\controller
 * @todo Catch exceptions
 */
class PageController
{
	/** @var PageApi */
	private $PageApi;
	/** @var PageView */
	private $PageView;
    /** @var UserApi */
    private $UserApi;

	public function __construct()
	{
		$this->PageApi = PageApiFactory::getPageApi();
		$this->PageView = new PageView(Postexus::getAlert());
        $this->UserApi = UserApiFactory::getUserApi();
	}

	public function home()
	{
        if ($this->UserApi->userIsAdmin()) {
            $pages = $this->PageApi->getPages();
            $this->PageView->home($pages);
        }
	}

    /**
     * @param int|null $pageId
     */
    public function addEdit($pageId = null)
	{
        if ($this->UserApi->userIsAdmin()) {
            if (is_null($pageId)) {
                $Page = $this->PageApi->createPage();
            } else {
                $Page = $this->PageApi->getPageById($pageId);
            }


            if (isset($_POST['submit'])) {
                $this->loadFormDataInPage($this->sanitizeFormData($_POST), $Page);

                try {
                    $this->PageApi->savePage($Page);
                    $this->PageView->saveSuccess();
                } catch (Exception $E) {
                    $this->PageView->saveFailure();
                }

                return;
            }

            $this->PageView->addEdit($Page);
            return;
        }
	}

    /**
     * @param int $pageId
     */
    public function remove($pageId)
    {
        if ($this->UserApi->userIsAdmin()) {
            $Page = $this->PageApi->getPageById($pageId);

            if (isset($_POST['submit'])) {
                try {
                    $this->PageApi->removePage($Page);
                    $this->PageView->removeSuccess();
                } catch (Exception $E) {
                    $this->PageView->removeFailure();
                }
                return;
            }

            $this->PageView->remove($Page);
            return;
        }
    }

    /**
     * @param $pageId
     */
    public function view($pageId)
    {
        if ($this->UserApi->userIsAdmin()) {
            $Page = $this->PageApi->getPageById($pageId);

            $this->PageView->view($Page);
            return;
        }
    }

    /**
     * @param array $formData
     * @param Page $Page
     */
    private function loadFormDataInPage(array $formData, Page $Page)
	{
        if (isset($formData['title'])) {
            $Page->setTitle($formData['title']);
        }

        if (isset($formData['content'])) {
            $Page->setContent($formData['content']);
        }

        if (isset($formData['page_is_published'])) {
            $Page->setIsPublished(true);
        } else {
            $Page->setIsPublished(false);
        }
	}

    /**
     * @param array $postData
     * @return array
     */
    private function sanitizeFormData(array $postData)
    {
        $allowedKeys = array_flip(array(
            'title',
            'content',
            'page_is_published'
        ));

        return array_intersect_key($postData, $allowedKeys);
    }
}