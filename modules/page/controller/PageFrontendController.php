<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\page\controller;

use postexus\core\Postexus;
use postexus\core\Response;
use postexus\modules\page\api\PageApi;
use postexus\modules\page\api\PageApiFactory;
use postexus\modules\page\view\PageFrontendView;

/**
 * Class PageController
 * @package postexus\modules\page\controller
 * @todo Catch exceptions
 */
class PageFrontendController
{
	/** @var PageApi */
	private $PageApi;
	/** @var PageFrontendView */
	private $PageFrontendView;
    /** @var Response */
    private $Response;

	public function __construct()
	{
		$this->PageApi = PageApiFactory::getPageApi();
		$this->PageFrontendView = new PageFrontendView();
        $this->Response = Postexus::getPostexus()->getResponse();
	}

    /**
     * @param int $pageId
     */
    public function view($pageId)
    {
        $Page = $this->PageApi->getPageById($pageId);
        $this->PageApi->setActivePageId($Page->getId());

        $this->Response->setHeader('/templates/default/core/frontendHeader.php');
        $this->Response->setFooter('/templates/default/core/frontendFooter.php');

        $this->PageFrontendView->view($Page);
        return;
    }
}