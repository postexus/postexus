<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\page\api;

use postexus\core\Config;
use postexus\modules\page\model\Page;
use postexus\modules\page\repository\PageRepository;
use postexus\modules\user\api\UserApi;

class PageApi
{
	/** @var PageRepository */
	private $PageRepository;
    /** @var Config */
    private $Config;
    /** @var UserApi */
    private $UserApi;

    /** @var null|int  */
    private $activePageId = null;

    /**
     * @param PageRepository $PageRepository
     * @param Config $Config
     * @param UserApi $UserApi
     */
    public function __construct(PageRepository $PageRepository, Config $Config, UserApi $UserApi)
	{
		$this->PageRepository = $PageRepository;
        $this->Config = $Config;
        $this->UserApi = $UserApi;
	}

    /**
     * @return Page
     */
    public function createPage()
	{
		return $this->PageRepository->createPage();
	}

	/**
	 * @param Page $Page
     * @return bool
     */
	public function savePage(Page $Page)
	{
        $pageAuthor = $Page->getAuthorId();
        if (empty($pageAuthor)) {
            $Page->setAuthorId($this->UserApi->getCurrentUser()->getId());
        }

		if (!$Page->validate()) {
            // TODO Display errors
        }

		return $this->PageRepository->savePage($Page);
	}

    /**
     * @param Page $Page
     * @return bool
     */
    public function removePage(Page $Page)
    {
        if ($Page->getId() != null) {
            return $this->PageRepository->removePage($Page);
        }

        return false;
    }

    /**
     * Returns an array of x pages, ordered by ID
     * @param int $numberOfPages
     * @return Page[]
     */
    public function getPages($numberOfPages = 20)
    {
        return $this->PageRepository->getObjects($numberOfPages);
    }

    /**
     * @param int $pageId
     * @return Page
     */
    public function getPageById($pageId)
    {
        return $this->PageRepository->getObjectById($pageId);
    }

    /**
     * @param int $pageId
     */
    public function setActivePageId($pageId)
    {
        $this->activePageId = $pageId;
    }

    /**
     * @return int|null
     */
    public function getActivePageId()
    {
        return $this->activePageId;
    }
}