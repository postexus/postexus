<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\dns\api;

use DateTime;
use postexus\modules\dns\model\Domain;
use postexus\modules\dns\repository\DomainRepository;

class DomainApi
{
    /** @var DomainRepository */
    private $DomainRepository;

    /**
     * @param DomainRepository $DomainRepository
     */
    public function __construct(DomainRepository $DomainRepository)
    {
        $this->DomainRepository = $DomainRepository;
    }

    /**
     * @return Domain
     */
    public function createDomain()
    {
        return $this->DomainRepository->createDomain();
    }

    /**
     * @param Domain $Domain
     */
    public function saveDomain(Domain $Domain)
    {
        // Get the current date and time
        $Today = new DateTime();

        // If the domain is not in need of an update yet, make sure it is now
        // If it is in need of an update, let it be
        if ($Domain->getNeedsUpdate() == false) {
            $LastChange = $Domain->getLastChange();
            $Diff = $LastChange->diff($Today);

            if ($Diff->d === 0) {
               $Domain->setSerialCount($Domain->getSerialCount() + 1);
            } else {
               $Domain->setSerialCount(1);
            }
            $Domain->setNeedsUpdate(true);
        }
        // Set lastChange here to prevent overwriting it before detecting a change in date for the serial count
        $Domain->setLastChange($Today);

        // TODO Try-catch and/or trigger?
        $Domain->validate();

        // TODO Try-catch
        $this->DomainRepository->saveDomain($Domain);
    }

    /**
     * @param Domain $Domain
     * @return bool
     */
    public function removeDomain(Domain $Domain)
    {
        if ($Domain->getId() != null) {
            return $this->DomainRepository->removeDomain($Domain);
        }

        return false;
    }

    /**
     * Returns an array of x domains, ordered by ID
     * @param int $numberOfDomains
     * @return \postexus\modules\dns\model\Domain[]
     */
    public function getDomains($numberOfDomains = 100)
    {
        return $this->DomainRepository->getObjects($numberOfDomains, 'domain_name');
    }

    /**
     * @param int $domainId
     * @return Domain|bool
     */
    public function getDomainById($domainId)
    {
        return $this->DomainRepository->getObjectById($domainId);
    }

    /**
     * @param string $domainName
     * @return mixed
     */
    public function getDomainByDomainName($domainName)
    {
        return $this->DomainRepository->getDomainByDomainName($domainName);
    }
}