<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\dns\api;

use postexus\modules\dns\model\Record;
use postexus\modules\dns\repository\RecordRepository;

class RecordApi
{
    /** @var RecordRepository */
    private $RecordRepository;

    /**
     * @param RecordRepository $RecordRepository
     */
    public function __construct(RecordRepository $RecordRepository)
    {
        $this->RecordRepository = $RecordRepository;
    }

    /**
     * @return Record
     */
    public function createRecord()
    {
        return $this->RecordRepository->createRecord();
    }

    /**
     * @param Record $Record
     */
    public function saveRecord(Record $Record)
    {
        // TODO Try-catch and/or trigger?
        $Domain = DnsApiFactory::getDomainApi()->getDomainById($Record->getDomainId());
        $Record->validate($Domain);

        // TODO Try-catch
        $this->RecordRepository->saveRecord($Record);
    }

    /**
     * @param Record $Record
     * @return bool
     */
    public function removeRecord(Record $Record)
    {
        if ($Record->getId() != null) {
            return $this->RecordRepository->removeRecord($Record);
        }

        return false;
    }

    /**
     * Returns an array of x domains, ordered by ID
     * @param int $domainId
     * @param int $numberOfRecords
     * @return \postexus\modules\dns\model\Record[]
     */
    public function getRecords($domainId, $numberOfRecords = 20)
    {
        return $this->RecordRepository->getRecords($domainId, $numberOfRecords);
    }

    /**
     * @param $domainId
     * @return Record|bool
     */
    public function getRecordById($domainId)
    {
        return $this->RecordRepository->getObjectById($domainId);
    }
}