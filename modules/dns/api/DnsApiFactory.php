<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\dns\api;

use postexus\core\Postexus;
use postexus\modules\dns\repository\DomainRepository;
use postexus\modules\dns\repository\RecordRepository;

class DnsApiFactory
{
    /** @var DomainApi */
    private static $DomainApi;
    /** @var RecordApi */
    private static $RecordApi;

    /**
     * @return DomainApi
     */
    public static function getDomainApi()
    {
        if (!self::$DomainApi) {
            self::$DomainApi = new DomainApi(new DomainRepository(Postexus::getDatabase()));
        }

        return self::$DomainApi;
    }

    /**
     * @return RecordApi
     */
    public static function getRecordApi()
    {
        if (!self::$RecordApi) {
            self::$RecordApi = new RecordApi(new RecordRepository(Postexus::getDatabase()));
        }

        return self::$RecordApi;
    }
}