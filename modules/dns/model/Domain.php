<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\dns\model;

use DateTime;
use postexus\core\base\Object;

class Domain extends Object
{
    /** @var int|null */
    protected $id = null;
    /** @var string */
    protected $domain_name;
    /** @var int */
    protected $owner_id;
    /** @var string */
    protected $soa_email;
    /** @var int */
    protected $refresh = 14400;
    /** @var int */
    protected $retry = 3600;
    /** @var int */
    protected $expiry = 1209600;
    /** @var int */
    protected $min_ttl = 3600;
    /** @var DateTime */
    protected $last_change = null;
    /** @var int */
    protected $serial_count = 1;
    /** @var int */
    protected $needs_update = 0;

    /** @var array */
    public $fieldMapping = array(
        'domainName' => 'domain_name',
        'ownerId' => 'owner_id',
        'soaEmail' => 'soa_email',
        'refresh' => 'refresh',
        'retry' => 'retry',
        'expiry' => 'expiry',
        'minTtl' => 'min_ttl',
        'lastChange' => 'last_change',
        'serialCount' => 'serial_count',
        'needsUpdate' => 'needs_update',
    );

    /**
     * @return string
     */
    public function getDomainName()
    {
        return $this->domain_name;
    }

    /**
     * @param string $domainName
     */
    public function setDomainName($domainName)
    {
        $this->domain_name = $domainName;
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $owner
     */
    public function setOwnerId($owner)
    {
        $this->owner_id = $owner;
    }

    /**
     * @return int
     */
    public function getOwnerId()
    {
        return $this->owner_id;
    }

    /**
     * @param int $expiry
     */
    public function setExpiry($expiry)
    {
        $this->expiry = $expiry;
    }

    /**
     * @return int
     */
    public function getExpiry()
    {
        return $this->expiry;
    }

    /**
     * @param DateTime $LastChange
     */
    public function setLastChange(DateTime $LastChange)
    {
        $this->last_change = $LastChange;
    }

    /**
     * @return DateTime
     */
    public function getLastChange()
    {
        if ($this->last_change instanceof DateTime) {
            return $this->last_change;
        } else {
            if (!empty($this->last_change)) {
                $this->last_change = new DateTime($this->last_change);
            } else {
                $this->last_change = new DateTime();
            }
        }

        return $this->last_change;
    }

    /**
     * @param boolean $needsUpdate
     */
    public function setNeedsUpdate($needsUpdate)
    {
        $this->needs_update = (bool) $needsUpdate;
    }

    /**
     * @return boolean
     */
    public function getNeedsUpdate()
    {
        return (bool) $this->needs_update;
    }

    /**
     * @param int $refresh
     */
    public function setRefresh($refresh)
    {
        $this->refresh = $refresh;
    }

    /**
     * @return int
     */
    public function getRefresh()
    {
        return $this->refresh;
    }

    /**
     * @param int $retry
     */
    public function setRetry($retry)
    {
        $this->retry = $retry;
    }

    /**
     * @return int
     */
    public function getRetry()
    {
        return $this->retry;
    }

    /**
     * @param int $serialCount
     */
    public function setSerialCount($serialCount)
    {
        $this->serial_count = $serialCount;
    }

    /**
     * @return int
     */
    public function getSerialCount()
    {
        return $this->serial_count;
    }

    /**
     * @param mixed $soaEmail
     */
    public function setSoaEmail($soaEmail)
    {
        $this->soa_email = $soaEmail;
    }

    /**
     * @return mixed
     */
    public function getSoaEmail()
    {
        return $this->soa_email;
    }

    /**
     * @param int $min_ttl
     */
    public function setMinTtl($min_ttl)
    {
        $this->min_ttl = $min_ttl;
    }

    /**
     * @return int
     */
    public function getMinTtl()
    {
        return $this->min_ttl;
    }

    /**
     * Very basic validation function
     * @return bool
     * @todo Check for data correctness and data types
     */
    public function validate()
    {
        if (empty($this->domain_name)) {
            //_('Domain name may not be empty');
            return false;
        }

        if (empty($this->owner_id)) {
            //_('Owner may not be empty');
            return false;
        }

        if (empty($this->soa_email)) {
            //_('SOA e-mail may not be empty');
            return false;
        }

        if (empty($this->refresh)) {
            //_('Refresh may not be empty');
            return false;
        }
        $this->refresh = (int) $this->refresh;

        if (empty($this->retry)) {
            //_('Retry may not be empty');
            return false;
        }
        $this->retry = (int) $this->retry;

        if (empty($this->expiry)) {
            //_('Expiry may not be empty');
            return false;
        }
        $this->expiry = (int) $this->expiry;

        if (empty($this->min_ttl)) {
            //_('Minimum TTL may not be empty');
            return false;
        }
        $this->min_ttl = (int) $this->min_ttl;

        /*if (!ctype_digit($this->serial_count)) {
            //_('Serial count must be an number');
        }*/
        $this->serial_count = (int) $this->serial_count;

        /*if (empty($this->needs_update)) {
            //_('Domain name may not be empty');
            return false;
        }*/
        $this->needs_update = (int) $this->needs_update;

        if (empty($this->last_change)) {
            $this->last_change = new DateTime();
        }

        return true;
    }
}
