<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\dns\model;

use postexus\core\base\Object;

class Record extends Object
{
    /** @var int|null */
    protected $id = null;
    /** @var int */
    protected $domain_id;
    /** @var string */
    protected $type;
    /** @var int */
    protected $ttl = null;
    /** @var int */
    protected $mx_priority = 10;
    /** @var string */
    protected $host;
    /** @var string */
    protected $content;

    const TYPE_A = 'A';
    const TYPE_AAAA = 'AAAA';
    const TYPE_CNAME = 'CNAME';
    const TYPE_MX = 'MX';
    const TYPE_NS = 'NS';
    const TYPE_SPF = 'SPF';
    const TYPE_SRV = 'SRV';
    const TYPE_TXT = 'TXT';

    /** @var array */
    private $recordTypes = array(
        self::TYPE_A => 'A',
        self::TYPE_AAAA => 'AAAA',
        self::TYPE_CNAME => 'CNAME',
        self::TYPE_MX => 'MX',
        self::TYPE_NS => 'NS',
        self::TYPE_SPF => 'SPF',
        self::TYPE_SRV => 'SRV',
	self::TYPE_TXT => 'TXT'
    );

    /** @var array */
    public $fieldMapping = array(
        'domainId' => 'domain_id',
        'type' => 'type',
        'ttl' => 'ttl',
        'mxPriority' => 'mx_priority',
        'host' => 'host',
        'content' => 'content',
    );

    /**
     * @return int
     */
    public function getDomainId()
    {
        return $this->domain_id ;
    }

    /**
     * @param int $domainId
     */
    public function setDomainId($domainId)
    {
        $this->domain_id  = $domainId;
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param int $mx_priority
     */
    public function setMxPriority($mx_priority)
    {
        $this->mx_priority = $mx_priority;
    }

    /**
     * @return int
     */
    public function getMxPriority()
    {
        return $this->mx_priority;
    }

    /**
     * @param int $ttl
     */
    public function setTtl($ttl)
    {
        $this->ttl = $ttl;
    }

    /**
     * @return int
     */
    public function getTtl()
    {
        return $this->ttl;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getRecordTypes()
    {
        return $this->recordTypes;
    }

    /**
     * Very basic validation function
     * @param Domain $Domain
     * @return bool
     * @todo Check for data correctness and data types
     */
    public function validate(Domain $Domain)
    {
        if (empty($this->domain_id)) {
            //_('Domain name must be set');
            return false;
        }

        if (empty($this->type) || !in_array($this->type, $this->recordTypes)) {
            //_('Type may not be empty');
            return false;
        }

        if (empty($this->ttl)) {
            $this->ttl = $Domain->getMinTtl();
        }
        $this->ttl = (int) $this->ttl;

        if ($this->type == 'MX' && empty($this->mx_priority)) {
            //_('Retry may not be empty');
            return false;
        }
        $this->mx_priority = (int) $this->mx_priority;

        if (empty($this->host)) {
            //_('Host may not be empty');
            return false;
        }

        if (empty($this->content)) {
            //_('Content may not be empty');
            return false;
        }

        return true;
    }
}
