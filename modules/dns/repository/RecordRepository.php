<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\dns\repository;

use Exception;
use PDO;
use postexus\core\base\Repository;
use postexus\modules\dns\model\Record;

class RecordRepository extends Repository
{
    /**
     * @return Record
     */
    public function createRecord()
    {
        return new Record();
    }

    /**
     * @param Record $Record
     * @throws Exception
     */
    public function saveRecord(Record $Record)
    {
        // Prepare statement
        if ($Record->getId() !== null) {
            $Statement = $this->Database->prepare("UPDATE " . $this->getTableName() . "
                                       SET " . $this->createUpdateClause($Record->fieldMapping) . "
                                       WHERE id = :id");
            $Statement->bindValue(':id', $Record->getId());
        } else {
            $Statement = $this->Database->prepare("INSERT INTO " . $this->getTableName() . " (" . implode(',', $Record->fieldMapping) . ")
                                       VALUES (:" . implode(', :', $Record->fieldMapping) . ")");
        }

        // Bind parameters
        foreach ($Record->fieldMapping as $objectField => $databaseField) {
            $method = 'get' . ucfirst($objectField);
            $Statement->bindValue(':' . $databaseField, $Record->$method());
        }

        if (!$Statement->execute()) {
            $errorInfo = $Statement->errorInfo();
            throw new Exception($Statement->errorCode() . ':' . $errorInfo[2]);
        }
    }

    /**
     * @param Record $Record
     * @return bool
     */
    public function removeRecord(Record $Record)
    {
        $Statement = $this->Database->prepare("DELETE
                FROM " . $this->getTableName() . "
                WHERE id = :id");
        $Statement->bindValue(':id', $Record->getId());

        return $Statement->execute();
    }

    /**
     * Returns an array of x records, ordered by ID
     * @param int $domainId
     * @return Record[]
     * @todo Use variable for limit
     */
    public function getRecords($domainId)
    {
        $Statement = $this->Database->prepare("SELECT *
                FROM " . $this->getTableName() . "
                WHERE domain_id = :domain_id
                ORDER BY host, type
                LIMIT 100");
        $Statement->bindValue(':domain_id', $domainId);

        $Statement->execute();

        return $Statement->fetchAll(PDO::FETCH_CLASS, $this->getClassName());
    }

    /**
     * Table name, should be escaped if necessary
     * @return string
     */
    protected function getTableName()
    {
        return '"record"';
    }

    /**
     * Class name including full and escaped namespace
     * @return string
     */
    protected function getClassName()
    {
        return "\\postexus\\modules\\dns\\model\\Record";
    }


}