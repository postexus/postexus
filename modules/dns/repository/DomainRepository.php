<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\dns\repository;

use Exception;
use PDO;
use postexus\core\base\Repository;
use postexus\modules\dns\model\Domain;

class DomainRepository extends Repository
{
    /**
     * @return Domain
     */
    public function createDomain()
    {
        return new Domain();
    }

    /**
     * @param Domain $Domain
     * @throws Exception
     */
    public function saveDomain(Domain $Domain)
    {
        // Prepare statement
        if ($Domain->getId() !== null) {
            $Statement = $this->Database->prepare("UPDATE " . $this->getTableName() . "
                                       SET " . $this->createUpdateClause($Domain->fieldMapping) . "
                                       WHERE id = :id");
            $Statement->bindValue(':id', $Domain->getId());
        } else {
            $Statement = $this->Database->prepare("INSERT INTO " . $this->getTableName() . " (" . implode(',', $Domain->fieldMapping) . ")
                                       VALUES (:" . implode(', :', $Domain->fieldMapping) . ")");
        }

        // Bind parameters
        foreach ($Domain->fieldMapping as $objectField => $databaseField) {
            if ($objectField == 'lastChange') {
                $Statement->bindValue(':' . $databaseField, $Domain->getLastChange()->format('Y-m-d H:i:s O'));
            } else {
                $method = 'get' . ucfirst($objectField);
                $value = $Domain->$method();

                if (is_bool($value)) {
                    $value = (int) $value;
                }

                $Statement->bindValue(':' . $databaseField, $value);
            }
        }

        if (!$Statement->execute()) {
            $errorInfo = $Statement->errorInfo();
            throw new Exception($Statement->errorCode() . ':' . $errorInfo[2]);
        }
    }

    /**
     * @param Domain $Domain
     * @return bool
     */
    public function removeDomain(Domain $Domain)
    {
        $Statement = $this->Database->prepare("DELETE
                FROM " . $this->getTableName() . "
                WHERE id = :id");
        $Statement->bindValue(':id', $Domain->getId());

        return $Statement->execute();
    }

    /**
     * @param string $domainName
     * @return mixed
     */
    public function getDomainByDomainName($domainName)
    {
        $Statement = $this->Database->prepare("SELECT *
                FROM " . $this->getTableName() . "
                WHERE domain_name = :domain_name");
        $Statement->bindValue(':domain_name', $domainName);

        $Statement->execute();

        $Statement->setFetchMode(PDO::FETCH_CLASS, $this->getClassName());

        return $Statement->fetch();
    }

    /**
     * Table name, should be escaped if necessary
     * @return string
     */
    protected function getTableName()
    {
        return '"domain"';
    }

    /**
     * Class name including full and escaped namespace
     * @return string
     */
    protected function getClassName()
    {
        return "\\postexus\\modules\\dns\\model\\Domain";
    }


}