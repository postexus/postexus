<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\dns\controller;

use postexus\core\Postexus;
use postexus\core\Response;
use postexus\modules\dns\api\DnsApiFactory;
use postexus\modules\dns\api\DomainApi;
use postexus\modules\dns\api\RecordApi;
use postexus\modules\dns\model\Record;
use postexus\modules\dns\view\RecordView;
use postexus\modules\user\api\UserApi;
use postexus\modules\user\api\UserApiFactory;

class RecordController
{
    /** @var RecordApi */
    private $RecordApi;
    /** @var UserApi */
    private $UserApi;
    /** @var DomainApi */
    private $DomainApi;
    /** @var RecordView */
    private $RecordView;
    /** @var Response */
    private $Response;

    public function __construct()
    {
        $this->RecordApi = DnsApiFactory::getRecordApi();
        $this->DomainApi = DnsApiFactory::getDomainApi();
        $this->UserApi = UserApiFactory::getUserApi();
        $this->RecordView = new RecordView(Postexus::getAlert());
        $this->Response = Postexus::getPostexus()->getResponse();
    }

    /**
     * @param int $domainId
     */
    public function home($domainId)
    {
        if ($this->UserApi->userIsAdmin()) {
            $records = $this->RecordApi->getRecords($domainId);
            $this->RecordView->home($records, $domainId);
        }
    }

    /**
     * @param int $domainId
     */
    public function add($domainId)
    {
        if ($this->UserApi->userIsAdmin()) {
            $Record = $this->RecordApi->createRecord();

            $Domain = $this->DomainApi->getDomainById($domainId);

            if (isset($_POST['submit']) || isset($_POST['submit-and-add'])) {
                $this->loadFormDataInRecord($_POST, $Record);

                $this->RecordApi->saveRecord($Record);

                $this->DomainApi->saveDomain($Domain);

                $this->RecordView->saveSuccess();

                if (!isset($_POST['submit-and-add'])) {
                    $this->Response->seeOther(Postexus::getUri('admin/dns/domain/records/' . $domainId));
                }
            }

            $this->RecordView->add($Record, $Domain);
            return;
        }
    }

    /**
     * @param int $recordId
     */
    public function edit($recordId)
    {
        if ($this->UserApi->userIsAdmin()) {
            $Record = $this->RecordApi->getRecordById($recordId);

            $Domain = $this->DomainApi->getDomainById($Record->getDomainId());

            if (isset($_POST['submit'])) {
                $this->loadFormDataInRecord($_POST, $Record);

                $this->RecordApi->saveRecord($Record);

                $this->DomainApi->saveDomain($Domain);

                $this->RecordView->saveSuccess();

                $this->Response->seeOther(Postexus::getUri('admin/dns/domain/records/' . $Domain->getId()));
            }

            $this->RecordView->edit($Record, $Domain);
            return;
        }
    }

    /**
     * @param int $recordId
     */
    public function remove($recordId)
    {
        if ($this->UserApi->userIsAdmin()) {
            $Record = $this->RecordApi->getRecordById($recordId);

            if (isset($_POST['submit'])) {
                $this->RecordApi->removeRecord($Record);

                $this->RecordView->removeSuccess();

                $this->Response->seeOther(Postexus::getUri('admin/dns/domain/records/' . $Record->getDomainId()));
            }

            $this->RecordView->remove($Record);
            return;
        }
    }

    /**
     * @param int $domainId
     */
    public function view($domainId)
    {
        if ($this->UserApi->userIsAdmin()) {
            $Record = $this->RecordApi->getRecordById($domainId);

            $this->RecordView->view($Record);
            return;
        }
    }

    /**
     * @param array $formData
     * @param Record $Record
     */
    private function loadFormDataInRecord(array $formData, Record $Record)
    {
        if (isset($formData['domain_id'])) {
            $Record->setDomainId($formData['domain_id']);
        }

        if (isset($formData['type'])) {
            $Record->setType($formData['type']);
        }

        if (isset($formData['ttl'])) {
            $Record->setTtl($formData['ttl']);
        }

        if (isset($formData['mx_priority'])) {
            $Record->setMxPriority($formData['mx_priority']);
        }

        if (isset($formData['host'])) {
            $Record->setHost($formData['host']);
        }

        if (isset($formData['content'])) {
            $Record->setContent($formData['content']);
        }
    }
}