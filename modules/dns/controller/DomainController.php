<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\dns\controller;

use DateTime;
use postexus\core\Postexus;
use postexus\modules\dns\api\DomainApi;
use postexus\modules\dns\api\DnsApiFactory;
use postexus\modules\dns\model\Domain;
use postexus\modules\dns\view\DomainView;
use postexus\modules\user\api\UserApi;
use postexus\modules\user\api\UserApiFactory;

class DomainController
{
    /** @var DomainApi */
    private $DomainApi;
    /** @var UserApi */
    private $UserApi;
    /** @var DomainView */
    private $DomainView;

    public function __construct()
    {
        $this->DomainApi = DnsApiFactory::getDomainApi();
        $this->UserApi = UserApiFactory::getUserApi();
        $this->DomainView = new DomainView(Postexus::getAlert());
    }

    public function home()
    {
        if ($this->UserApi->userIsAdmin()) {
            $domains = $this->DomainApi->getDomains();
            $this->DomainView->home($domains);
        }
    }

    public function add()
    {
        if ($this->UserApi->userIsAdmin()) {
            $Domain = $this->DomainApi->createDomain();

            if (isset($_POST['submit'])) {
                $this->loadFormDataInDomain($_POST, $Domain);

                $this->DomainApi->saveDomain($Domain);
                $this->DomainView->saveSuccess();
                return;
            }

            $users = $this->UserApi->getUsers();

            $this->DomainView->add($Domain, $users, $this->UserApi->userIsAdmin());
            return;
        }
    }

    public function edit($domainId)
    {
        if ($this->UserApi->userIsAdmin()) {
            $Domain = $this->DomainApi->getDomainById($domainId);

            if (isset($_POST['submit'])) {
                $this->loadFormDataInDomain($_POST, $Domain);

                $this->DomainApi->saveDomain($Domain);
                $this->DomainView->saveSuccess();
                return;
            }

            $users = $this->UserApi->getUsers();

            $this->DomainView->edit($Domain, $users, $this->UserApi->userIsAdmin());
            return;
        }
    }

    /**
     * @param int $domainId
     */
    public function remove($domainId)
    {
        if ($this->UserApi->userIsAdmin()) {
            $Domain = $this->DomainApi->getDomainById($domainId);

            if (isset($_POST['submit'])) {
                $this->DomainApi->removeDomain($Domain);
                $this->DomainView->removeSuccess();
                return;
            }

            $this->DomainView->remove($Domain);
            return;
        }
    }

    /**
     * @param int $domainId
     */
    public function view($domainId)
    {
        if ($this->UserApi->userIsAdmin()) {
            $Domain = $this->DomainApi->getDomainById($domainId);

            $this->DomainView->view($Domain);
            return;
        }
    }

    /**
     * @param array $formData
     * @param Domain $Domain
     */
    private function loadFormDataInDomain(array $formData, Domain $Domain)
    {
        if (isset($formData['domain_name'])) {
            $Domain->setDomainName($formData['domain_name']);
        }

        if (isset($formData['owner'])) {
            $Domain->setOwnerId($formData['owner']);
        }

        if (isset($formData['soa_email'])) {
            $Domain->setSoaEmail($formData['soa_email']);
        }

        if (isset($formData['refresh'])) {
            $Domain->setRefresh($formData['refresh']);
        }

        if (isset($formData['retry'])) {
            $Domain->setRetry($formData['retry']);
        }

        if (isset($formData['expiry'])) {
            $Domain->setExpiry($formData['expiry']);
        }

        if (isset($formData['min_ttl'])) {
            $Domain->setMinTtl($formData['min_ttl']);
        }

        if ($this->UserApi->userIsAdmin()) {
            if (isset($formData['last_change'])) {
                $Domain->setLastChange(new DateTime($formData['last_change']));
            }

            if (isset($formData['serial_count'])) {
                $Domain->setSerialCount($formData['serial_count']);
            }

            if (isset($formData['needs_update'])) {
                $Domain->setNeedsUpdate(true);
            } else {
                $Domain->setNeedsUpdate(false);
            }
        }
    }
}