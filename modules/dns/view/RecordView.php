<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\modules\dns\view;

use postexus\helpers\alert\Alert;
use postexus\helpers\alert\AlertApi;
use postexus\modules\dns\model\Domain;
use postexus\modules\dns\model\Record;

class RecordView
{
    /** @var Alert */
    private $Alert;

    /**
     * @param \postexus\helpers\alert\Alert|\postexus\helpers\alert\AlertApi $Alert $Alert
     * @todo Alert should be the AlertApi
     */
    public function __construct(AlertApi $Alert)
    {
        $this->Alert = $Alert;
    }

    /**
     * @param array $records
     * @param int $domainId
     */
    public function home($records, $domainId)
    {
        require_once(BASEDIR . '/templates/default/modules/dns/listRecords.php');
    }

    /**
     * @param Record $Record
     * @return string
     */
    public function view(Record $Record)
    {
        require_once(BASEDIR . '/templates/default/modules/dns/removeRecord.php');
    }

    /**
     * @param Record $Record
     * @param Domain $Domain
     */
    public function add(Record $Record, Domain $Domain)
    {
        require_once(BASEDIR . '/templates/default/modules/dns/addEditRecord.php');
    }

    /**
     * @param Record $Record
     * @param Domain $Domain
     */
    public function edit(Record $Record, Domain $Domain)
    {
        require_once(BASEDIR . '/templates/default/modules/dns/addEditRecord.php');
    }

    /**
     * @param Record $Record
     */
    public function remove(Record $Record)
    {
        require_once(BASEDIR . '/templates/default/modules/dns/removeRecord.php');
    }

    public function saveSuccess()
    {
        $this->Alert->addAlert(Alert::TYPE_SUCCESS, _('Record successfully saved!'));
    }

    public function removeSuccess()
    {
        $this->Alert->addAlert(Alert::TYPE_SUCCESS, _('Record successfully removed!'));
    }
}

?>
