<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// The namespacing is just for code completion, it doesn't do much on the CLI
namespace postexus;

use postexus\core\Postexus;
use postexus\modules\user\api\UserApiFactory;


// Define the current directory as the base directory for the rest of the request
define('BASEDIR', __DIR__);

// Include required libraries
// TODO Remove when PHP 5.5 is common
require_once(BASEDIR . '/libraries/password_compat/password_compat.php');
require_once(BASEDIR . '/modules/user/api/UserApiFactory.php');
require_once(BASEDIR . '/modules/user/api/UserApi.php');
require_once(BASEDIR . '/core/Config.php');
require_once(BASEDIR . '/core/base/Repository.php');
require_once(BASEDIR . '/core/base/Object.php');
require_once(BASEDIR . '/modules/user/repository/UserRepository.php');
require_once(BASEDIR . '/modules/user/model/User.php');

// Include the core (called Postexus)
require_once(BASEDIR . '/core/Postexus.php');

$DB = Postexus::getDatabase();

$Statement = $DB->prepare('CREATE TABLE "user"
(
  id serial NOT NULL,
  username character varying,
  email_address character varying,
  password character varying,
  is_admin integer,
  CONSTRAINT user_id PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);');

if (!$Statement->execute()) {
    var_dump($Statement->errorInfo());
}

$Statement = $DB->prepare('CREATE TABLE domain
(
  id serial NOT NULL,
  domain_name character varying,
  refresh integer,
  retry integer,
  expiry integer,
  soa_email character varying,
  min_ttl integer,
  last_change timestamp with time zone,
  serial_count integer,
  needs_update integer,
  owner_id integer,
  CONSTRAINT domain_id PRIMARY KEY (id),
  CONSTRAINT domain_owner_id FOREIGN KEY (owner_id)
      REFERENCES "user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);');

if (!$Statement->execute()) {
    var_dump($Statement->errorInfo());
}

$Statement = $DB->prepare('CREATE INDEX fki_domain_owner_id
  ON domain
  USING btree
  (owner_id);');

if (!$Statement->execute()) {
    var_dump($Statement->errorInfo());
}

$Statement = $DB->prepare('CREATE TABLE record
(
  id serial NOT NULL,
  type character varying,
  ttl integer,
  mx_priority integer,
  host character varying,
  content character varying,
  domain_id integer,
  CONSTRAINT record_id PRIMARY KEY (id),
  CONSTRAINT record_domain_id FOREIGN KEY (domain_id)
      REFERENCES domain (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);');

if (!$Statement->execute()) {
    var_dump($Statement->errorInfo());
}

$Statement = $DB->prepare('CREATE INDEX fki_record_domain_id
  ON record
  USING btree
  (domain_id);');

if (!$Statement->execute()) {
    var_dump($Statement->errorInfo());
}

$Statement = $DB->prepare('ALTER TABLE "user"
  ADD COLUMN is_active integer;');

if (!$Statement->execute()) {
    var_dump($Statement->errorInfo());
}

$Statement = $DB->prepare('CREATE TABLE page
(
  id serial NOT NULL,
  title character varying,
  content text,
  last_change timestamp with time zone,
  is_published integer,
  author_id integer,
  CONSTRAINT page_id PRIMARY KEY (id),
  CONSTRAINT page_author_id FOREIGN KEY (author_id)
      REFERENCES "user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);');

if (!$Statement->execute()) {
    var_dump($Statement->errorInfo());
}

$Statement = $DB->prepare('ALTER TABLE page
  ADD COLUMN author_id integer;');

if (!$Statement->execute()) {
    var_dump($Statement->errorInfo());
}

$Statement = $DB->prepare('CREATE INDEX fki_page_author_id
  ON page
  USING btree
  (author_id);');


if (!$Statement->execute()) {
    var_dump($Statement->errorInfo());
}

$UserApi = UserApiFactory::getUserApi();
if (count($UserApi->getUsers()) < 1) {
    $username = readline("Username for admin user:\n");

    $emailAddress = readline("E-mail address for admin user:\n");

    echo "Password for admin user:\n";
    system('stty -echo');
    $password = trim(fgets(STDIN));
    system('stty echo');

    $User = $UserApi->createUser();
    $User->setUsername($username);
    $User->setEmailAddress($emailAddress);
    $User->setNewPassword($password);
    $User->setNewPasswordConfirmation($password);
    $User->setIsAdmin(true);
    $User->setIsActive(true);

    if (!$UserApi->saveUser($User)) {
        echo "Could not create user!\n";
    }
}

echo "Thanks!\n";