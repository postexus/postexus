<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use postexus\core\Postexus;

/**
 * @todo Create some sort of output class in order to clean up the mess below
 */

// Start timing the request
$start = microtime();

// Define the current directory as the base directory for the rest of the request
define('BASEDIR', __DIR__);

// Include required libraries
// TODO Remove when PHP 5.5 is common
require_once(BASEDIR . '/libraries/password_compat/password_compat.php');

// Include the core (called Postexus)
require_once(BASEDIR . '/core/Postexus.php');

$Postexus = Postexus::getPostexus();

// TODO Auto-load several core classes?

// Load Router and dispatch request
$Router = \postexus\core\Postexus::getRouter();
$Router->dispatchRequest();
$Postexus->getResponse()->outputResponse();

// TODO Prettify
// Echo request time
//$requestTime =  microtime() - $start;
//echo $requestTime;