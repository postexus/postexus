<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\core;

class Config
{
    /** @var string */
    private $database_dsn;
    /** @var string */
    private $database_username;
    /** @var string */
    private $database_password;
    /** @var array */
    private $modules;
    /** @var string */
    private $urlPrefix;
    /** @var string */
    private $cookieDomain;

    // TODO Remove after testing
    /** @var array Array containing all raw config options */
    public $config;

    public function __construct()
    {
        $config = yaml_parse_file(BASEDIR . '/config.yaml');

        if (isset($config['database'])) {
            if (isset($config['database']['dsn'])) {
                $this->database_dsn = $config['database']['dsn'];
            }

            if (isset($config['database']['username'])) {
                $this->database_username = $config['database']['username'];
            }

            if (isset($config['database']['password'])) {
                $this->database_password = $config['database']['password'];
            }
        }

        if (isset($config['modules'])) {
            $this->modules = $config['modules'];
        }

        if (isset($config['urlPrefix'])) {
            $this->urlPrefix = $config['urlPrefix'];
        }

        if (isset($config['cookieDomain'])) {
            $this->cookieDomain = $config['cookieDomain'];
        }

        // TODO Remove after testing
        $this->config = $config;
    }

    public function getModules()
    {
        return $this->modules;
    }

    public function getUrlPrefix()
    {
        return $this->urlPrefix;
    }

    public function getDatabaseDsn()
    {
        return $this->database_dsn;
    }

    public function getDatabasePassword()
    {
        return $this->database_password;
    }

    public function getDatabaseUsername()
    {
        return $this->database_username;
    }

    /**
     * @return string
     */
    public function getCookieDomain()
    {
        return $this->cookieDomain;
    }
}