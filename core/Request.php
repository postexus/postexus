<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\core;

class Request
{
    /** @var bool */
    private $isAdminRequest = false;
    /** @var string */
    private $module;
    /** @var string */
    private $requestUrl;

    public function __construct()
    {
        $Config 	= Postexus::getConfig();
        $prefix     = $Config->getUrlPrefix();

        // See if there is a prefix to parse it off the start of the URL
        $start = 1;
        if (!empty($prefix)) {
            $start = strlen($Config->getUrlPrefix()) + 2;
        }

        $requestUrl = substr($_SERVER['REQUEST_URI'], $start);
        $urlParts 	= explode('/', $requestUrl);

        $moduleIndex = 0;
        if (isset($urlParts[0]) && $urlParts[0] == 'admin') {
            $this->isAdminRequest = true;
            $moduleIndex = 1;
        }

        if (isset($urlParts[$moduleIndex]) && $this->isModule($urlParts[$moduleIndex])) {
            // Set module
            $this->module 	= $urlParts[$moduleIndex];

            // Strip url and directory prefix off URL and save it
            if ($this->isAdminRequest) {
                $this->requestUrl = 'admin/' . substr($requestUrl, strlen($this->module) + 7);
            } else {
                $this->requestUrl = substr($requestUrl, strlen($this->module) + 1);
            }
        } else {
            $this->module = 'core';

            if ($this->isAdminRequest) {
                $this->requestUrl = 'admin';
            }
        }
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

	/**
	 * @return string
	 */
	public function getRequestUrl()
    {
        return $this->requestUrl;
    }

    /**
     * @param string $module
     * @return bool
     */
    private function isModule($module)
    {
        $Config = Postexus::getConfig();
        $modules = $Config->getModules();

        return in_array($module, $modules);
    }

    /**
     * @return boolean
     */
    public function getIsAdminRequest()
    {
        return $this->isAdminRequest;
    }
}