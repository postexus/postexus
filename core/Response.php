<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\core;

class Response
{
    /** @var string */
    private $headerFilePath = '/templates/default/core/backendHeader.php';
    /** @var string */
    private $footerFilePath = '/templates/default/core/backendFooter.php';

    public function __construct()
    {
        // Start output buffering
        ob_start();
    }

    public function send404()
    {
        if ((substr(php_sapi_name(), 0, 3) == 'cgi')) {
            header("Status: 404 Not Found");
        } else {
            header("HTTP/1.1 404 Not Found");
        }
    }

    public function redirectTemporary($targetUrl)
    {
        header("Location: " . $targetUrl, true, 302);
    }

    /**
     * Sends a location header (HTTP 303) with the given URL
     * @param string $targetUrl
     */
    public function seeOther($targetUrl)
    {
        header("Location: " . $targetUrl, true, 303);
    }

    public function outputResponse()
    {
        // Save content
        $content = ob_get_contents();
        ob_clean();

        // TODO Move to view class
        // Output header
        require_once(BASEDIR . $this->headerFilePath);

        // Output alert(s)
        $Alert = Postexus::getAlert();
        $Alert->outputAlerts();

        // Output contents
        echo $content;

        // TODO Move to view class
        // Output footer
        require_once(BASEDIR . $this->footerFilePath);

        // Flush the buffer
        ob_end_flush();
    }

    /**
     * File path to the header file, relative to BASEDIR with an opening slash
     * @param string $filePath
     * @todo Check/sanitize
     */
    public function setHeader($filePath)
    {
        $this->headerFilePath = $filePath;
    }

    /**
     * File path to the footer file, relative to BASEDIR with an opening slash
     * @param string $filePath
     * @todo Check/sanitize
     */
    public function setFooter($filePath)
    {
        $this->footerFilePath = $filePath;
    }
}