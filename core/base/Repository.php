<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\core\base;

use PDO;

abstract class Repository
{
    /** @var PDO */
    protected $Database;

    public function __construct(PDO $Database)
    {
        $this->Database = $Database;
    }

    /**
     * @param array $fieldMapping
     * @return string
     */
    public function createUpdateClause($fieldMapping)
    {
        // Create set string
        $clause = '';
        foreach ($fieldMapping as $field) {
            $clause .= $field . ' = :' . $field . ',';
        }
        $clause = substr($clause, 0, strlen($clause) - 1);

        return $clause;
    }

    /**
     * @param int $objectId
     * @return mixed
     */
    public function getObjectById($objectId)
    {
        $Statement = $this->Database->prepare("SELECT *
                FROM " . $this->getTableName() . "
                WHERE id = :id");
        $Statement->bindValue(':id', $objectId);

        $Statement->execute();

        $Statement->setFetchMode(PDO::FETCH_CLASS, $this->getClassName());

        return $Statement->fetch();
    }

    /**
     * Returns an array of x pages, ordered by ID
     * @param $resultLimit
     * @param null $orderBy
     * @return Object[]
     * @todo The orderBy should be done differently, as now it's prone to possible SQL injections
     */
    public function getObjects($resultLimit = null, $orderBy = null)
    {
        $sql = "SELECT *
                FROM " . $this->getTableName();

        if (!is_null($orderBy)) {
            $sql .= " ORDER BY " . $orderBy . " ASC";
        }

        if (!is_null($resultLimit)) {
            $sql .= " LIMIT :resultLimit";
        }

        $Statement = $this->Database->prepare($sql);

        if (!is_null($resultLimit)) {
            $Statement->bindValue(':resultLimit', (int) trim($resultLimit), PDO::PARAM_INT);
        }

        $Statement->execute();

        return $Statement->fetchAll(PDO::FETCH_CLASS, $this->getClassName());
    }

    /**
     * @return string
     */
    protected abstract function getTableName();

    /**
     * @return string
     */
    protected abstract function getClassName();
}