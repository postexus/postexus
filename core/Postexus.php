<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\core;
use PDO;
use postexus\helpers\alert\Alert;
use postexus\helpers\alert\AlertApi;
use postexus\modules\user\model\User;

class Postexus
{
    /** @var Postexus */
    private static $Postexus = null;
    /** @var PDO */
    private static $Database;
    /** @var User */
    private static $User;
    /** @var Config */
    private static $Config;
    /** @var Request */
    private static $Request;
    /** @var Router */
    private static $Router;
    /** @var Response */
    private $Response;

    // Helpers
    /** @var Alert */
    private static $Alert;

    private function __construct()
    {
        // Set custom auto-loader
        spl_autoload_register(array('\postexus\core\Postexus', 'autoLoad'));
    }

    /**
     * @return Postexus
     */
    public static function getPostexus()
    {
        if (is_null(self::$Postexus)) {
            self::$Postexus = new Postexus();
            self::$Postexus->loadResponse();
        }

        return self::$Postexus;
    }

    /**
     * @static
     * @return \PDO $Database
     */
    public static function getDatabase()
    {
        $Config = self::getConfig();

        if (!isset(self::$Database)) {
            self::$Database = new \PDO($Config->getDatabaseDsn(), $Config->getDatabaseUsername(), $Config->getDatabasePassword());
        }

        return self::$Database;
    }

    /**
     * @static
     * @return User $User
     */
    public static function getUser()
    {
        if (!isset(self::$User)) {
            // TODO Should this be here?
            //$UserController = new UserController();
            //$UserController->checkLogin();
        }

        return self::$User;
    }

    /**
     * @param User $User
     */
    public static function setUser(User $User)
    {
        self::$User = $User;
    }

    /**
     * @static
     * @return \postexus\core\Config $Config
     */
    public static function getConfig()
    {
        if (!isset(self::$Config)) {
            self::$Config = new Config();
        }

        return self::$Config;
    }

    /**
     * @static
     * @return \postexus\core\Request $Request
     */
    public static function getRequest()
    {
        if (!isset(self::$Request)) {
            self::$Request = new Request();
        }

        return self::$Request;
    }

    /**
     * @static
     * @return \postexus\core\Router $Router
     */
    public static function getRouter()
    {
        if (!isset(self::$Router)) {
            self::$Router = new Router();
        }

        return self::$Router;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->Response;
    }

    private function loadResponse()
    {
        $this->Response = new Response();
    }

    /**
     * @static
     * @return \postexus\helpers\alert\AlertApi
     */
    public static function getAlert()
    {
        if (!isset(self::$Alert)) {
            self::$Alert = new AlertApi();
        }

        return self::$Alert;
    }

    /**
     * @param string $class
     * @return bool
     */
    public static function autoLoad($class)
    {
        $path = explode("\\", $class);
        if (array_shift($path) == 'postexus') {
            require_once(implode('/', $path) . '.php');
        }

        return false;
    }

    /**
     * @param string|null $path
     * @return string
     */
    public static function getUri($path = null)
    {
        $Config = self::getConfig();

        $prefix = $Config->getUrlPrefix();

        if (empty($prefix)) {
            $uri = '/' . $path;
        } else {
            $uri = '/' . $prefix . '/' . $path;
        }

        return $uri;
    }
}