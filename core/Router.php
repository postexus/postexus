<?php
/**
 * Postexus
 * Copyright (C) 2012 - 2015 Maarten Kossen (mpkossen), Quateria
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace postexus\core;

use postexus\modules\user\api\UserApi;
use postexus\modules\user\api\UserApiFactory;

class Router
{
    /** @var array */
    private $routes;
    /** @var Request */
    private $Request;
    /** @var Response */
    private $Response;
    /** @var UserApi */
    private $UserApi;

    public function __construct()
    {
        $routes = array();
        $Config = Postexus::getConfig();

        foreach ($Config->getModules() as $module) {
            $file = BASEDIR . '/modules/' . $module . '/routes.yaml';

            if (file_exists($file)) {
                $routes[$module] = yaml_parse_file($file);
            }
        }

        $routes['core'] = yaml_parse_file(BASEDIR . '/routes.yaml');

        $this->routes = $routes;
        $this->Request 	= Postexus::getRequest();
        $this->Response = Postexus::getPostexus()->getResponse();
        $this->UserApi = UserApiFactory::getUserApi();
    }

    /**
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @todo Implement a fancy 404
     * Forwards the request to the proper module/class/function or headers a 404
     */
    public function dispatchRequest()
    {
        $module 	= $this->Request->getModule();
        $requestUrl = $this->Request->getRequestUrl();
        $callData	= array();

        if (!$this->Request->getIsAdminRequest() || ($this->Request->getIsAdminRequest() && $this->UserApi->userIsAdmin()) || ($module == 'user' && ($requestUrl == 'login' || $requestUrl == 'register'))) {
            // Don't even try finding a route when there's no route data for the module
            if (isset($this->routes[$module])) {
                // Loop through routes fo the request module and try to match a pattern
                foreach ($this->routes[$module]['routes'] as $route) {
                    if (isset($route['pattern'])) {
                        if ($matches = $this->matchPattern($route['pattern'], $requestUrl)) {
                            $callData = $route;
                            $callData['parameters'] = array_slice($matches, 1);
                            break;
                        }
                    }
                }

                // Try and find the default route if there is no callData
                if (empty($callData)) {
                    if (isset($this->routes[$module]['default'])) {
                        $callData = $this->routes[$module]['default'];
                        $callData['parameters'] = array();
                    }
                }

                // If there is callData, try to perform a call
                if (!empty($callData)) {
                    if (isset($callData['class']) && isset($callData['function'])) {
                        $Controller = new $callData['class']();
                        call_user_func_array(array($Controller, $callData['function']), $callData['parameters']);
                        return;
                    }
                }
            } else {
                // Default controller

            }
        }

        if ($this->UserApi->userIsGuest()) {
            $this->Response->redirectTemporary(Postexus::getUri('user/login'));
        } else {
            $this->Response->send404();
        }
    }

    /**
     * @param string $pattern The pattern to match the URL against (this will be made a regular expression)
     * @param string $requestUrl The request URL
     * @return array|bool Array with matches if any found, false otherwise
     * Try to match a pattern to the current request URL
     */
    private function matchPattern($pattern, $requestUrl)
    {
        $regex = '@' . str_replace('/', '\/', $pattern) . '@';

        if (preg_match($regex, $requestUrl, $matches)) {
            return $matches;
        }

        return false;
    }
}